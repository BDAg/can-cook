import requests
from bs4 import BeautifulSoup
import csv
contPage = 0
cont = 0
cont2 = 0


with open('dados_energéticos_alimentos.csv', mode='w', encoding='utf-8', newline='') as csv_file2:
    list_head = ['ID', 'Nome', 'Grupo','Componente', 'Unidades', 'Valor por 100 g']
    writer = csv.DictWriter(csv_file2, fieldnames=list_head)
    writer.writeheader()

    for x in range(1,6):
        for y in range(1,11):
            cont=0
            contPage+=1

            if (contPage>43):
                print("Bot chegou na última página")
                break
            else:
                allPage = requests.get('http://www.tbca.net.br/base-dados/composicao_alimentos.php?pagina={}&atuald={}'.format(contPage,x))
                print(allPage.status_code + contPage)
                allSoup = BeautifulSoup(allPage.text, 'html.parser')
                table_body = allSoup.find('tbody')

                while (cont <= (len(table_body) - 1)):
                    valueList = table_body.contents[cont].find_all('td')

                    codigo = valueList[0].text
                    page = requests.get('http://www.tbca.net.br/base-dados/int_composicao_alimentos.php?cod_produto={}'.format(codigo))
                    soup = BeautifulSoup(page.text, 'html.parser')

                    registros = soup.find('tbody')
                    writerDict = {}

                    cont2 = 0
                    while (cont2 < (len(registros))):
                        writerDict.update({f'{list_head[0]}':f'{valueList[0].text}'})
                        writerDict.update({f'{list_head[1]}':f'{valueList[1].text}'})
                        writerDict.update({f'{list_head[2]}':f'{valueList[4].text}'})
                        unidades = registros.contents[cont2].find_all('td')
                        for x in range(3,len(list_head)):
                            f = x - 3
                            writerDict.update({f'{list_head[x]}':f'{unidades[f].text}'})    


                        writer.writerow(writerDict)    
                        print(writerDict) 
                        
                        cont2 += 1

                    cont += 1

